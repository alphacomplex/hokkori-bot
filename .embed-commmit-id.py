#!/usr/bin/env python
# -*- coding: utf-8 -*-s
""""Smudg clean filter."""

import sys
import re
import subprocess

GITCOMMITID = None


def smuge(line):
    """Sumudge."""
    line = re.sub(r'"\$GCID\$"', r'"$GCID: {}$"'.format(GITCOMMITID), line)
    return line


def clean(line):
    """Clean."""
    line = re.sub(r'"\$GCID: [a-z0-9]+\$"', r'"$GCID$"', line)
    return line


def null(line):
    """Null function."""
    return line


def main():
    """Main Main Main."""
    global GITCOMMITID
    proc = subprocess.Popen('git rev-parse HEAD', stdout=subprocess.PIPE, shell=True)
    GITCOMMITID = proc.stdout.read().decode('UTF-8').strip()

    if sys.argv[1] == 'smudge':
        filter_func = smuge
    elif sys.argv[1] == 'clean':
        filter_func = clean
    else:
        filter_func = null

    for line in sys.stdin:
        print(filter_func(line), end='')

if __name__ == '__main__':
    main()
