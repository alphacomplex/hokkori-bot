#!/usr/bin/env python

from __future__ import print_function
from __future__ import unicode_literals

from rtmbot.core import Plugin
import slackclient as sc
from logging import getLogger, NullHandler, StreamHandler, Formatter, INFO, DEBUG
import re
from sqlalchemy import create_engine, text
from tabulate import tabulate


class SQLPlugin(Plugin):
    """俳句を詠め."""

    def __init__(self, name=None, slack_client=None, plugin_config=None):
        """Init baby!."""
        super().__init__(name, slack_client, plugin_config)

        self.version = 'v1'
        self.db = None
        self.config = plugin_config

        # このクラスだけのLoggerを設定してあげる
        self.logger = getLogger(self.__class__.__name__)
        formatter = Formatter(fmt='%(asctime)s %(name)s %(levelname)5s:%(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S%z',)

        if self.config.get('DEBUG', False):
            handler = StreamHandler()
            handler.formatter = formatter
            self.logger.addHandler(handler)
            handler.setLevel(DEBUG if self.config.get('DEBUG', False) else INFO)

        self.logger.info('Starting SQLplugin')
        self.logger.debug(self.config)

    def process_hello(self, data):
        """Called when the plugin connects."""
        self.db = create_engine('sqlite:///slackbot.db')


    def process_message(self, data):
        """Process the message like a boss."""
        if not isinstance(data, dict) or 'text' not in data:
            self.logger.error('got an odd looking message .... {}'.format(data))
            return

        if re.search(u'^SQL\n', data['text']):
            query = '\n'.join(data['text'].split('\n')[1:]).strip("`")
            result = ''
            try:
                results = self.db.engine.execute(query)
                result = tabulate(results, headers=results.keys() )
            except Exception as e:
                result = e

            self.outputs.append(
                    [data['channel'], "Query:```{}```\nResult:```{}```".format(query,result)]
            )
