#!/usr/bin/env python
"""短歌を詠むプラグイン."""

from __future__ import print_function
from __future__ import unicode_literals

from rtmbot.core import Plugin
import slackclient as sc
from logging import getLogger, NullHandler, StreamHandler, Formatter, INFO, DEBUG

from oauth2client.service_account import ServiceAccountCredentials
from apiclient import discovery

import re
from collections import defaultdict
import random


class TankaPlugin(Plugin):
    """俳句を詠め."""

    def __init__(self, name=None, slack_client=None, plugin_config=None):
        """Init baby!."""
        super().__init__(name, slack_client, plugin_config)

        self.tanka_version = 'v1'
        self.index = {}
        self.tanka = {}
        self.config = plugin_config

        # このクラスだけのLoggerを設定してあげる
        self.logger = getLogger(self.__class__.__name__)
        formatter = Formatter(fmt='%(asctime)s %(name)s %(levelname)5s:%(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S%z',)

        if self.config.get('DEBUG', False):
            handler = StreamHandler()
            handler.formatter = formatter
            self.logger.addHandler(handler)
            handler.setLevel(DEBUG if self.config.get('DEBUG', False) else INFO)

        self.logger.info('Starting Haiku')
        self.logger.debug(self.config)

    def load_remote_tanka(self, version):
        """Load Tanka from Remote Location."""
        self.logger.info('Fetching GoogleDocs Tanka....')

        # GoogleDocにアクセスするためのアカウント情報を読み込む
        credentials_file = self.config.get('GOOGLE_CREDENTIALS_FILE', 'hokkori-bot.json')
        credentials = ServiceAccountCredentials.from_json_keyfile_name(credentials_file)

        # GoogleDocに接続して、シートAPI、version4を使用することを伝える
        service = discovery.build('sheets', 'v4', credentials=credentials)

        # どのシートを読み込むのかを指定する。
        spreadsheetId = '1BQyw7V1Se3TKTLstR-BkcW5qK51b3HhDf6fne0s5cvg'

        # インデックスを読み込む
        index_range = '{sheetName}!A1:F7'.format(sheetName=version)
        result = service.spreadsheets().values().get(spreadsheetId=spreadsheetId, range=index_range).execute()
        index = result.get('values', [])

        data_range = '{sheetName}_data!A:F'.format(sheetName=version)
        result = service.spreadsheets().values().get(spreadsheetId=spreadsheetId, range=data_range).execute()
        data = result.get('values', [])

        # 何もなかった場合にはその旨をログって、あきらめる。 この場合、みくじデーターに変更はない。
        if not index:
            self.logger.error('No Tanka data found at temote GoogleDocs Sheets.')
            return

        # 最初の行は無視する。　２行目以降にはどの表を選ぶのが入ってる
        # [ "roll", "1句目の表","2句目の表","3句目の表","4句目の表"　.... ]
        for row in index[1:]:
            self.index[int(row[0])] = row[1:]

        data_iter = iter(data)
        for row in data_iter:
            self.logger.info(row)

            m = re.search(u'【第(\d)句】', row[0])

            # 該当しないデータは無視。次いこ、次。
            if not m:
                continue

            # ここに来たってことは該当するヘッダがあったということなので、
            # まずは何句目なのかを抜き出して、self.tankaに格納するスペースを作ります。
            idx = int(m.group(1))  # 何句目かを表す
            self.tanka[idx] = defaultdict(dict)

            # 空行があるまでデータを読み込む
            for line in data_iter:
                # 我、空行発見セリ
                if not line:
                    break

                # 一列目と、2列目が対に、3列目と4列目が対になってるので、
                # 対になってる奴をヘッダで示されている名前の所に放りこむ
                # 1列目と3列目が数字なので、数字に変換しておく。
                self.tanka[idx][row[1]][int(line[0])] = line[1]
                self.tanka[idx][row[3]][int(line[2])] = line[3]

        # debug
        self.logger.debug('-' * 80)
        for idx in sorted(self.tanka):
            table = self.tanka[idx]
            self.logger.debug('{}句'.format(idx))
            for key in ['い表', 'ろ表']:
                for widx in sorted(table[key]):
                    self.logger.debug('{} {}: {:2d} {}'.format(idx, key, widx, table[key][widx]))

        self.logger.info('Fetched Tanka: version={}'.format(version))

    @staticmethod
    def rollDie(number):
        """xD6 を振る."""
        return sum(random.randint(1, 6) for _ in range(number))

    def read_tanka(self):
        """短歌を詠え."""
        tanka = []
        table_idx = self.rollDie(1)
        for idx, table in enumerate(self.index[table_idx], 1):
            word_idx = self.rollDie(2)

            self.logger.info('{} {} {}'.format(idx, table, word_idx))

            tanka.append(self.tanka[idx][table][word_idx])
        return tanka

    def process_hello(self, data):
        """Called when the plugin connects."""
        # GoogleDocから短歌を読み込む
        self.load_remote_tanka(self.tanka_version)

    def process_message(self, data):
        """Process the message like a boss."""
        if not isinstance(data, dict) or 'text' not in data:
            self.logger.error('got an odd looking message .... {}'.format(data))
            return

        if re.search(u'ここで一句', data['text']):

            try:
                tanka = '　'.join(self.read_tanka())

                self.outputs.append(
                    [data['channel'], '{}'.format(tanka)]
                )
                self.logger.info('responded to input {data[text]} on channel {data[channel]} with {tanka}'.format(data=data, tanka=tanka))
            except Exeption as e:
                self.outputs.append(
                    [
                        data['channel'], 'う、詩を詠もうとしたら恐ろしいもんをみちまった...\n{}'.format(e)
                    ]
                )
                self.logger.error('Unable to generate Tanka.', exc_info=True)
