#!/usr/bin/env python
"""Simple repat plugin."""

from __future__ import print_function
from __future__ import unicode_literals

from rtmbot.core import Plugin
from logging import getLogger, NullHandler, StreamHandler, Formatter, INFO, DEBUG

import urllib
from pprint import pprint

from random import random
from bisect import bisect
from datetime import datetime, timedelta

import requests
import json


class Splatoon2Plugin(Plugin):
    """Splatoon Class."""

    
    bearer = """Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MDA5MDc5MzUsImlzcyI6ImFwaS1scDEuem5jLnNydi5uaW50ZW5kby5uZXQiLCJpYXQiOjE1MDA5MDA3MzUsInR5cCI6ImlkX3Rva2VuIiwiYXVkIjoiZjQxN2UxdGlianFkOTFjaDk5dTQ5aXd6NXNuOWNoeTMiLCJzdWIiOjQ2NDg3NjI1MDkwOTkwMDh9.x3Zk2httHFjfPX48GQCAc6Tm63NDKRscyxHDj2e7378"""
    resource_id = {"id": 5741031244955648}

    schedules = {}

    def __init__(self, name=None, slack_client=None, plugin_config=None):
        """Init baby!."""
        super().__init__(name, slack_client, plugin_config)

        self.splatoon2_version = 'v1'
        self.config = plugin_config

        # このクラスだけのLoggerを設定してあげる
        self.logger = getLogger(self.__class__.__name__)
        formatter = Formatter(fmt='%(asctime)s %(name)s %(levelname)5s:%(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S%z',)

        if self.config.get('DEBUG', False):
            handler = StreamHandler()
            handler.formatter = formatter
            self.logger.addHandler(handler)
            handler.setLevel(DEBUG if self.config.get('DEBUG', False) else INFO)

        self.session = None
        self.session_expire = None

        self.logger.info('Starting Splatoon2')
        self.logger.debug(self.config)

    def fill_in_users(self):
        """Fill the usermap with info from the remote side."""
        # ユーザー名とIDを取ってくる。
        # まとめて取っておくので、途中でユーザー名がかわっても名前は変わらない...。

        for user in self.slack_client.api_call("users.list")["members"]:
            self.users[user["id"]] = user["name"]


    def set_session(self):

        now = datetime.now()

        if self.session is None or self.session_expire < now:
            session = requests.Session()
            response = session.post('https://accounts.nintendo.com/connect/1.0.0/api/token', headers={'Accept': 'application/json'},
                                    json={"client_id": "71b963c1b7b6d119",
                                          "grant_type": "urn:ietf:params:oauth:grant-type:jwt-bearer-session-token",
                                          "session_token": "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MDA5MDA3MzEsImlzcyI6Imh0dHBzOi8vYWNjb3VudHMubmludGVuZG8uY29tIiwic3ViIjoiMzY1YzVkNjA1NDgwMjUyZCIsInN0OnNjcCI6WzAsOCw5LDE3LDIzXSwiZXhwIjoxNTYzOTcyNzMxLCJ0eXAiOiJzZXNzaW9uX3Rva2VuIiwianRpIjoiMTE2NDE4Mzg1IiwiYXVkIjoiNzFiOTYzYzFiN2I2ZDExOSJ9.j6uzifEOmiclxzHJMNAqEFNVdJKcXR5pB7FKVF_Wq2Q"
                                    })
            access_tokens = response.json()
            #print(json.dumps(access_tokens, indent=4))
            response = session.post('https://api-lp1.znc.srv.nintendo.net/v1/Account/GetToken',
                                    headers={'Accept': 'application/json',  
                                             'Authorization': "Bearer " + access_tokens["access_token"]},
                                    json = {"parameter": {
                                            "language": 'null',
                                            "naBirthday": 'null',
                                            "naCountry": 'null',
                                            "naIdToken": access_tokens["id_token"] }
                                    }) 
            tokens = response.json()["result"]
            #print(json.dumps(response.json(),indent=4))
            #print(session.cookies)
    

            response = session.post('https://api-lp1.znc.srv.nintendo.net/v1/Game/GetWebServiceToken', 
                                    headers={'Accept': 'application/json', 
                                             'Authorization': "Bearer "+tokens["webApiServerCredential"]["accessToken"]}, 
                                    json={"parameter": self.resource_id} )
            res_json = response.json()
            if res_json["status"] != 0:
                self.logger.error(json.dumps(res_json,indent=4))
                raise RuntimeError("initial auth failed")
            access_token = res_json["result"]["accessToken"]
            token_expires_in = res_json["result"]["expiresIn"]
            self.session_expire = datetime.now() + timedelta(seconds=res_json["result"]["expiresIn"] - 300 ) 
            self.logger.info("Session Expires at {}".format(self.session_expire))
            # get the cookie setup 
            response = session.get("https://app.splatoon2.nintendo.net/?lang=ja-JP", headers={'accept': 'application/json', 'X-gamewebtoken': access_token})
            self.session = session





    def process_hello(self, data):
        """Called when the plugin connects."""
        self.set_session()

    def process_message(self, data):
        """Process the message like a boss."""
        if not isinstance(data, dict) or 'text' not in data:
            self.logger.error('got an odd looking message .... {}'.format(data))
            return

        if ('イカ' in data['text']) and \
           ('ナワバリ' in data['text']):
            try:
                self.set_session()

                msg = ""
                res = self.session.get('https://app.splatoon2.nintendo.net/api/schedules', headers={'accept': 'application/json'})
                for stages in res.json()["regular"][0:2]:
                    msg += "{} : {:%Y-%m-%d %H:%M} - {:%Y-%m-%d %H:%M}\n".format(urllib.parse.unquote(stages["rule"]["name"]), datetime.fromtimestamp(stages["start_time"]),datetime.fromtimestamp(stages["end_time"]))
                    msg += " {} / {} \n".format( urllib.parse.unquote(stages["stage_a"]["name"]), urllib.parse.unquote(stages["stage_b"]["name"]) )
                    #self.logger.debug(stages)
                
            except Exception as e:
                self.logger.error('Exception when processing Splatoon Schedules.',exc_info=True)
                msg = 'イカしてない状況なんです。\n{}'.format(e)

            self.logger.debug(msg)
            self.outputs.append([data['channel'], msg])



