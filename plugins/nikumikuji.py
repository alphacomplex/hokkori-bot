#!/usr/bin/env python
"""Simple repat plugin."""

from __future__ import print_function
from __future__ import unicode_literals

from rtmbot.core import Plugin
from logging import getLogger, NullHandler, StreamHandler, Formatter, INFO, DEBUG

from pprint import pprint

from random import random
from bisect import bisect
from datetime import datetime, timedelta

from oauth2client.service_account import ServiceAccountCredentials
from apiclient import discovery


class NikuMikujiPlugin(Plugin):
    """NiKuMikuji Class."""

    mikuji = [
        (0.1, '大吉'), (0.2, '吉'), (0.3, '中吉'),
        (0.4, '小吉'), (0.3, '半吉'), (0.3, '末吉'),
        (0.3, '末小吉'), (0.2, '平'), (0.2, '凶'),
        (0.2, '小凶'), (0.2, '半凶'), (0.1, '末凶'), (0.05, '大凶'),
    ]

    parts = [
        (0.1, 'タン'), (0.2, 'かしら'),
        (0.3, 'ハラミ'), (0.5, 'カルビ'),
        ]

    remote_mikuji = {}

    last_mikuji = {}
    users = {}

    def __init__(self, name=None, slack_client=None, plugin_config=None):
        """Init baby!."""
        super().__init__(name, slack_client, plugin_config)

        self.mikuji_version = 'v1'
        self.config = plugin_config

        # このクラスだけのLoggerを設定してあげる
        self.logger = getLogger(self.__class__.__name__)
        formatter = Formatter(fmt='%(asctime)s %(name)s %(levelname)5s:%(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S%z',)

        if self.config.get('DEBUG', False):
            handler = StreamHandler()
            handler.formatter = formatter
            self.logger.addHandler(handler)
            handler.setLevel(DEBUG if self.config.get('DEBUG', False) else INFO)

        self.logger.info('Starting NikuMikuji')
        self.logger.debug(self.config)


    @staticmethod
    def int_or_float(string):
        """Convert string to a int or a float with the preference of it being an int."""
        try:
            return int(string)
        except ValueError:
            return float(string)

    @staticmethod
    def weighted_choices(choices):
        """Select a weigted opition."""
        weights, values = zip(*choices)
        total = 0
        cum_weights = []
        for w in weights:
            total += w
            cum_weights.append(total)
        x = random() * total
        i = bisect(cum_weights, x)
        return values[i]

    def fill_in_users(self):
        """Fill the usermap with info from the remote side."""
        # ユーザー名とIDを取ってくる。
        # まとめて取っておくので、途中でユーザー名がかわっても名前は変わらない...。

        for user in self.slack_client.api_call("users.list")["members"]:
            self.users[user["id"]] = user["name"]

    def load_remote_mikuji(self, version):
        """Load Mikuji from Remote Location."""
        self.logger.info('Fetching GoogleDocs NikuMikuji....')

        # GoogleDocにアクセスするためのアカウント情報を読み込む
        credentials_file = self.config.get('GOOGLE_CREDENTIALS_FILE', 'hokkori-bot.json')
        credentials = ServiceAccountCredentials.from_json_keyfile_name(credentials_file)

        # GoogleDocに接続して、シートAPI、version4を使用することを伝える
        service = discovery.build('sheets', 'v4', credentials=credentials)

        # どのシートを読み込むのかを指定する。
        spreadsheetId = '13mFwUcCssJiB7Kdv6gSa57T-cNsS7aKHik7SrahRBZ0'
        rangeName = '{sheetName}!A2:B'.format(sheetName=version)

        # 指定したシートの中身を読み込む
        result = service.spreadsheets().values().get(spreadsheetId=spreadsheetId, range=rangeName).execute()
        values = result.get('values', [])

        # 何もなかった場合にはその旨をログって、あきらめる。 この場合、みくじデーターに変更はない。
        if not values:
            self.logger.error('No data found at temote GoogleDocs Sheets.')
            return

        # データーがあった場合には現在のみくじデータを破棄して読み込み直す
        self.remote_mikuji[version] = []

        for row in values:
            try:
                self.remote_mikuji[version].append((self.int_or_float(row[0]), row[1]))
            except:
                self.logger.warn('unable to load row: {}'.format(row))

        self.logger.info('Fetched GoogleDocs NikuMikuji: version={}, count={}'.format(version,len(self.remote_mikuji[version])))

    def get_mikuji(self, data):
        """Process the message and post back to the user a mikuji."""
        now = datetime.now()       # 何度も調べなくて良いように現在時間をとっとく
        userid = data['user']      # ユーザーidのショートカット
        delta = timedelta(hours=20)  # 肉みくじを次ひ引けるまでの時間差

        # ユーザー名の分からないユーザーがいたら、新規参加者なので、名前を調べとこう。
        if userid not in self.users:
            self.fill_in_users()

        # 知っている or 調べたばかりなので、ハズレはいないハズ。
        username = self.users[userid]

        # 肉みくじを引いた事があり、それが次に引けるまでの時間差内だった場合には、
        # 肉みくじが引けない事をと次にいつ引けるかを教える。
        if userid in self.last_mikuji and \
           self.last_mikuji[userid] + delta >= now:

            # 時間差を計算しておく。
            # timedelta は日数（days)と秒数(seconds)で構成されてるので、
            # 時間と分数は計算してあげる。
            until = self.last_mikuji[userid] + delta - now
            msg = 'おじいちゃん、今日はもうにくみじくを引いたでしょう....。一日一回までよ。 次にひけるのは{}時間{}分後よ。'.format(until.seconds//3600, (until.seconds//60) % 60)

            return msg

        # 肉みくじを引いた事を記録しておく。
        self.last_mikuji[userid] = now

        if self.mikuji_version in self.remote_mikuji and \
           self.remote_mikuji[self.mikuji_version] and self.mikuji_version == 'v1':

            msg = self.weighted_choices(self.remote_mikuji[self.mikuji_version])

        else:
            # 肉みくじを選んで、投稿者に返信する。
            luck = self.weighted_choices(self.mikuji)
            part = self.weighted_choices(self.parts)

            msg = '{}さんの運勢は【{}】です。お勧めの部位は【{}】です'.format(username, luck, part)

        return msg

    def process_hello(self, data):
        """Called when the plugin connects."""
        # スタート時にユーザー名を収集してとっとく。
        self.fill_in_users()

        # GoogleDocからみくじを読み込む
        self.load_remote_mikuji(self.mikuji_version)

    def process_message(self, data):
        """Process the message like a boss."""
        if not isinstance(data, dict) or 'text' not in data:
            self.logger.error('got an odd looking message .... {}'.format(data))
            return

        if ('にくみくじ' in data['text'] or '肉みくじ' in data['text']) and \
           ('リロード' in data['text'] or 'reload' in data['text']):
            try:
                self.load_remote_mikuji(self.mikuji_version)
                self.last_mikuji = {}
                msg = '{}個の肉みくじをGoogleDocsからロードしました。'.format(len(self.remote_mikuji[self.mikuji_version]))  
            except Exception as e:
                self.logger.error('Exception when generating Mikuji.',exc_info=True)
                msg = '旦那ぁ、申し訳ねぇんですが、エラーがおきやして.....\n{}'.format(e)

            self.outputs.append([data['channel'], msg])

        elif ('にくみくじ' in data['text'] or '肉みくじ' in data['text']):
            msg = self.get_mikuji(data)
            self.outputs.append([data['channel'], msg])
