#!/usr/bin/env python
""" simple repat plugin. """

from __future__ import print_function
from __future__ import unicode_literals

from rtmbot.core import Plugin
import slackclient as sc
import logging
from pprint import pprint

class RepeatPlugin(Plugin):

    """ Repeat Plugin Class. """

    def __init__(self, name=None, slack_client=None, plugin_config=None):
        super().__init__(name, slack_client, plugin_config)
        logging.info('Starting RepeatPlugin')

    def process_hello(self, data):

        pprint({self.__class__.__name__+'hello': data})

    def process_message(self, data):
        """ process the message like a boss. """

        # pprint((self.__class__.__name__, data))

        return


        if 'print users' in data['text']:
            for user in sc.api_call("users.list")["members"]:
                print(user["name"], user["id"])

        elif data['channel'].startswith("D"):
            self.outputs.append(
                [data['channel'], 'from repeat1 "{}" in channel {}'.format(
                    data['text'], data['channel']
                )]
            )
            print('responded to input {text} on channel {channel}'.format(**data))

        