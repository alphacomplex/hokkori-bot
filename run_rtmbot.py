#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Command line tool."""

from argparse import ArgumentParser
import sys
import os
import yaml
from copy import deepcopy
from rtmbot import RtmBot

from websocket._exceptions import WebSocketConnectionClosedException

# declare version number.

__version__ = '0.0.1'
__build__ = "$GCID$"

# add current directory to PYTHONPATH
sys.path.append(os.getcwd())

DEFAILT_RTMCONF_FILE = 'rtmbot.conf'
DEFAULT_SECRETS_FILE = 'secrets-site.conf'
DEFAULT_LOGGER_NAME = 'HokkoriBot'


def parse_args():
    """Process Args like a boss."""
    parser = ArgumentParser()
    parser.add_argument(
        '-c',
        '--config',
        help='Full path to config file. default is: {}'.format(DEFAILT_RTMCONF_FILE),
        default=DEFAILT_RTMCONF_FILE,
        metavar='path'
    )
    parser.add_argument(
        '-s',
        '--secrets',
        help='Full path to Slack Token config file. default is: {}'.format(DEFAULT_SECRETS_FILE),
        default=DEFAULT_SECRETS_FILE,
        metavar='path'
    )
    return parser.parse_args()


def merge(target, addition):
    """Deep Merge dicts."""
    obj = addition
    if not isinstance(obj, dict):
        return obj
    for k, v in obj.items():
        if k in target and isinstance(target[k], dict):
            merge(target[k], v)
        else:
            target[k] = deepcopy(v)
    return target


def main(args=None):
    """Main func."""
    # load args with config path if not specified
    if not args:
        args = parse_args()

    base = yaml.load(open(args.config, 'r'))
    secrets = yaml.load(open(args.secrets, 'r'))
    config = merge(base, secrets)

    def run_bot():
        # start the bot
        bot = RtmBot(config)
        try:
            bot.start()
        except KeyboardInterrupt:
            sys.exit(0)
    
    while True:
        try:
            run_bot()
        except WebSocketConnectionClosedException as e:
            print(e,file=sys.stderr)


if __name__ == "__main__":
    main()
